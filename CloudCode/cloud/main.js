
Parse.Cloud.beforeSave("_User", function(request, response) {
  if (!request.object.get("employees")) {
    request.object.set("employees", []);
  }

  if (!request.object.get("zones")) {
    request.object.set("zones", []);
  }

  response.success();
});
