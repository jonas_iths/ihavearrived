//
//  AddZoneCellController.swift
//  Ihavearrived
//
//  Created by Jonas on 03/11/15.
//  Copyright © 2015 Jonas. All rights reserved.
//

import UIKit
import Parse

class AddZoneCellController: UITableViewCell, UITextFieldDelegate {
 
    
    @IBOutlet weak var iBeaconString: UITextField!
    @IBOutlet weak var iBeaconNameString: UITextField!
    var parent:addZoneController!
    
    @IBAction func addZoneButton(sender: AnyObject) {
        
        // if textfields are filled in, if zone !exists, add zone
        // loop through and check if zone already exists
        
        if (iBeaconString.text! != "" &&  iBeaconNameString.text! != "") {
            
            let user = PFUser.currentUser()
            var arr = user!["zones"] as! [[String]]

            var shouldAdd = true
            
            for zone in arr {
                if zone[0] == iBeaconString.text! {
                    shouldAdd = false
                    self.parent.alertAlreadySavedZone()
                    break;
                }
            }
            
            if shouldAdd {
                arr.append([iBeaconString.text!,iBeaconNameString.text!])
                user!["zones"] = arr
                self.parent.arrayForTable = arr
                user!.saveInBackground()
                
                print(self.parent.arrayForTable)
                
                self.parent.reload()
            }

        } else {
            parent.alertEmptyTextField()
        }
        
    }
    
    @IBOutlet weak var iBeaconNameCell2: UILabel!
    @IBOutlet weak var iBeaconIDCell2: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
