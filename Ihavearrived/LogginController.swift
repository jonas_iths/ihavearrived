//
//  LogginController.swift
//  Ihavearrived
//
//  Created by Jonas on 21/10/15.
//  Copyright © 2015 Jonas. All rights reserved.
//

import UIKit
import Parse

class LogginController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var yourEmail: UITextField?
    @IBOutlet weak var yourPassword: UITextField?
    @IBOutlet weak var loading: UIActivityIndicatorView!
    
    @IBAction func employeBtn(sender: AnyObject) {
    
        yourEmail!.text = "j@j.se"
        yourPassword!.text = "j"
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        yourEmail!.delegate = self
        yourPassword!.delegate = self
        loading.layer.cornerRadius = 50
    }

    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func changeLoggin(sender: AnyObject) {
        
        yourEmail!.text = "jonas@jonasekstrom.se"
        yourPassword!.text = "jonas"
        
    }
    
    @IBAction func okLoginButton(sender: AnyObject) {
        
        if (yourEmail!.text == "" || yourPassword!.text! == "") {
            
            let alertController = UIAlertController(title: "Oooops", message:
                "Fill in properly please!", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default,handler: nil))
            self.presentViewController(alertController, animated: true, completion: nil)
            
        } else {
            
            stopShowLoading()
            view.userInteractionEnabled = false
            
            PFUser.logOut()
            
            PFUser.logInWithUsernameInBackground(yourEmail!.text!.lowercaseString, password: yourPassword!.text!) {
                (user: PFUser?, error: NSError?) -> Void in
                if user != nil {
                    
                    let user = PFUser.currentUser()
                    let isUserManager = user!["manager"] as! Bool
                    if (isUserManager) {
                        
                        self.stopShowLoading()
                        self.view.userInteractionEnabled = true
                        self.performSegueWithIdentifier("toManagerSegue", sender: nil)
                    
                    } else {
                        
                        let date = NSDate()
                        let cal = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
                        let newDate = cal.startOfDayForDate(date)
                        
                        // print(date)
                        
                        let user = PFUser.currentUser()
                        
                        let query = PFQuery(className:"Checkin")
                        query.whereKey("owner", equalTo:user!.objectId!)
                        query.whereKey("createdAt", greaterThan:newDate)
                        query.findObjectsInBackgroundWithBlock {
                            (objects: [PFObject]?, error: NSError?) -> Void in
                            
                            if error == nil {
                                // print("Successfully retrieved \(objects!.count) objects")
                                
                                if objects!.count % 2 == 0{
                                    sharedClass.checkedInLastTime = false
                                } else {
                                    sharedClass.checkedInLastTime = true
                                }
                                
                                // check if a boss added me and add those iBeacon uuid
                                let query = PFUser.query()
                                query!.whereKey("employees", equalTo:user!.objectId!)
                                // let objectIdAsString = user!.objectId! as String
                                
                                query!.findObjectsInBackgroundWithBlock {
                                    (objects: [PFObject]?, error: NSError?) -> Void in
                                    
                                    if error == nil {
                                        
                                        sharedClass.zones = []
                                        
                                        for object in objects! {
                                            
                                            let zones = object["zones"] as! [[String]]
                                            
                                            // print(zones)
                                            
                                            for obj in zones {
                                                
                                                if sharedClass.zones.contains(obj[0]) {
                                                    print("already in array")
                                                } else {
                                                    sharedClass.zones.append(obj[0])
                                                }
                                            }
                                        }
                                        self.stopShowLoading()
                                        self.view.userInteractionEnabled = true
                                        self.performSegueWithIdentifier("toCheckinSegue", sender: nil)
                                        
                                    } else {
                                        // Log details of the failure
                                        self.stopShowLoading()
                                        self.view.userInteractionEnabled = true
                                        print("Error: \(error!) \(error!.userInfo)")
                                    }
                                }
                                
                            } else {
                                // Log details of the failure
                                self.stopShowLoading()
                                self.view.userInteractionEnabled = true
                                print("Error: \(error!) \(error!.userInfo)")
                            }
                        }
                        
                    }
                    
                } else {
                    // The login failed. Check error to see why.
                    print("the error", error)
                    self.stopShowLoading()
                    self.view.userInteractionEnabled = true
                    
                    let alertController = UIAlertController(title: "Oooops", message:
                        "Wrong email or password!", preferredStyle: UIAlertControllerStyle.Alert)
                    alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default,handler: nil))
                    self.presentViewController(alertController, animated: true, completion: nil)

                }
            }
            
        }
        
    }
    
    func stopShowLoading() {
        
        if loading.isAnimating() {
            loading.stopAnimating()
        } else {
            loading.startAnimating()
        }
        
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
       override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    
    
    }


}
