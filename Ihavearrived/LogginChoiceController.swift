//
//  LogginChoiceController.swift
//  Ihavearrived
//
//  Created by Jonas on 21/10/15.
//  Copyright © 2015 Jonas. All rights reserved.
//

import UIKit
import Parse

class LogginChoiceController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()                
        
    }
    
    override func viewWillAppear(animated: Bool) {
        
        navigationController!.navigationBar.barTintColor = UIColor(red: 51.0/255, green: 51.0/255, blue: 51.0/255, alpha: 1.0)
        
        navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor(red: 255.0/255, green: 255.0/255, blue: 255.0/255, alpha: 1.0)]
        
        navigationController!.navigationBar.tintColor = UIColor(red: 255.0/255, green: 255.0/255, blue: 255.0/255, alpha: 1.0)
        
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
            imageView.contentMode = .ScaleAspectFit
            // 4
            let image = UIImage(named: "navItem")
            imageView.image = image
            // 5
            navigationItem.titleView = imageView

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        navigationItem.backBarButtonItem = backItem // This will show in the next view controller being pushed
        
        if (segue.identifier == "registerManagerSegue") {
            sharedClass.manager = true
        } else {
            sharedClass.manager = false
        }
   
    }
    
}
