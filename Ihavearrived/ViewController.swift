//
//  ViewController.swift
//  Ihavearrived
//
//  Created by Jonas on 21/10/15.
//  Copyright © 2015 Jonas. All rights reserved.
//

import UIKit
import CoreLocation
import Parse
import AVFoundation

class ViewController: UIViewController, CLLocationManagerDelegate {

    let locationManager = CLLocationManager()
    var currentZone = ""
    let region = CLBeaconRegion(proximityUUID: NSUUID(UUIDString: "B9407F30-F5F8-466E-AFF9-25556B57FE6D")!, identifier: "SprintaHQ")
    var imageForButtonIsCheckin = true
    
    @IBOutlet weak var signInButton: UIButton!
    
    @IBOutlet weak var signInButtonHeight: NSLayoutConstraint!
        
    @IBOutlet weak var signInButtonWidth: NSLayoutConstraint!
    
    @IBOutlet weak var loading: UIActivityIndicatorView!
    
    let beepSoundURL =  NSBundle.mainBundle().URLForResource("bell", withExtension: "wav")!
    var beepPlayer = AVAudioPlayer()
    
    func playMySound(){
        
        do {
        try
            beepPlayer = AVAudioPlayer(contentsOfURL: beepSoundURL, fileTypeHint: nil)
            beepPlayer.prepareToPlay()
            beepPlayer.play()
        } catch {
            print("sound error")
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        locationManager.delegate = self;
        
        loading.layer.cornerRadius = 50
        
        if (CLLocationManager.authorizationStatus() != CLAuthorizationStatus.AuthorizedWhenInUse) {
            locationManager.requestWhenInUseAuthorization()
        }
        
        var count = 0
        
        for regions in sharedClass.zones {
            
            let uuid = NSUUID(UUIDString: regions)
            // print(uuid)
            
            if uuid != nil {
                
                let oneRegion = CLBeaconRegion(proximityUUID: NSUUID(UUIDString: regions)!, identifier: String(count))
                locationManager.startRangingBeaconsInRegion(oneRegion)
                count++
                
            }
            
        }
        
        // locationManager.startRangingBeaconsInRegion(region)
        // loop through and start monitoring
        
        if (!sharedClass.checkedInLastTime) {
            // signInButton.setTitle("Check in", forState: UIControlState.Normal)
            signInButton.setImage(UIImage(named: "checkInBtn.png"), forState: .Normal)
            imageForButtonIsCheckin = true
        } else {
            // signInButton.setTitle("Check out", forState: UIControlState.Normal)
            signInButton.setImage(UIImage(named: "checkOutBtn.png"), forState: .Normal)
            imageForButtonIsCheckin = false
        }
        
    }
    
    func changeButtonTitle() {
        
        if imageForButtonIsCheckin == true {
            // signInButton.setTitle("Check out", forState: UIControlState.Normal)
            imageForButtonIsCheckin = false
            signInButton.setImage(UIImage(named: "checkOutBtn.png"), forState: .Normal)
        } else {
            imageForButtonIsCheckin = true
            // signInButton.setTitle("Check in", forState: UIControlState.Normal)
            signInButton.setImage(UIImage(named: "checkInBtn.png"), forState: .Normal)
        }
        
    }
    
    func ltzOffset() -> Int { return NSTimeZone.localTimeZone().secondsFromGMT }
    
    override func viewWillAppear(animated: Bool) {
        
        signInButton.enabled = false
        signInButton.alpha = 0.3

        signInButtonHeight.constant = view.frame.size.width - 80
        signInButtonWidth.constant = view.frame.size.width - 80
        signInButton.layer.cornerRadius = (view.frame.size.width - 80) / 2
        
    }
    
    override func viewDidAppear(animated: Bool) {
        
    }
    
    func stopShowLoading() {
        
        /*
        UIView.animateWithDuration(2.0, animations: {
            self.imageView.transform = CGAffineTransformMakeRotation((180.0 * CGFloat(M_PI)) / 180.0)
        })
        */
        
        if loading.isAnimating() {
            loading.stopAnimating()
        } else {
            loading.startAnimating()
        }
        
    }
    
    func updateAppDidBecomeActive() {
        
        stopShowLoading()
        
        let date = NSDate()
        let cal = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
        let newDate = cal.startOfDayForDate(date)
        
        let user = PFUser.currentUser()
        
        let query = PFQuery(className:"Checkin")
        query.whereKey("owner", equalTo:user!.objectId!)
        query.whereKey("createdAt", greaterThan:newDate)
        query.findObjectsInBackgroundWithBlock {
            (objects: [PFObject]?, error: NSError?) -> Void in
            
            if error == nil {
                // print("Successfully retrieved \(objects!.count) objects")
                
                if objects!.count % 2 == 0{
                    sharedClass.checkedInLastTime = false
                } else {
                    sharedClass.checkedInLastTime = true
                }
                
                if (!sharedClass.checkedInLastTime) {
                    // signInButton.setTitle("Check in", forState: UIControlState.Normal)
                    self.signInButton.setImage(UIImage(named: "checkInBtn.png"), forState: .Normal)
                    self.imageForButtonIsCheckin = true
                } else {
                    // signInButton.setTitle("Check out", forState: UIControlState.Normal)
                    self.signInButton.setImage(UIImage(named: "checkOutBtn.png"), forState: .Normal)
                    self.imageForButtonIsCheckin = false
                }
                
                // check if a boss added me and add those iBeacon uuid
                let query = PFUser.query()
                query!.whereKey("employees", equalTo:user!.objectId!)
                // let objectIdAsString = user!.objectId! as String
                
                query!.findObjectsInBackgroundWithBlock {
                    (objects: [PFObject]?, error: NSError?) -> Void in
                    
                    if error == nil {
                        
                        sharedClass.zones = []
                        
                        for object in objects! {
                            
                            let zones = object["zones"] as! [[String]]
                            
                            for obj in zones {
                                
                                if sharedClass.zones.contains(obj[0]) {
                                    print("already in array")
                                } else {
                                    sharedClass.zones.append(obj[0])
                                }
                            }
                        }
                        self.stopShowLoading()
                        self.view.userInteractionEnabled = true
                        self.performSegueWithIdentifier("toCheckinSegue", sender: nil)
                        
                    } else {
                        // Log details of the failure
                        self.stopShowLoading()
                        self.view.userInteractionEnabled = true
                        print("Error: \(error!) \(error!.userInfo)")
                    }
                }
                
            } else {
                // Log details of the failure
                self.stopShowLoading()
                self.view.userInteractionEnabled = true
                print("Error: \(error!) \(error!.userInfo)")
            }
        }
    
    }
    
    @IBAction func signInButton(sender: AnyObject) {
        
        // print ( ltzOffset() )
        
        playMySound()
        
        stopShowLoading()
        let user = PFUser.currentUser()
        
        let checkin = PFObject(className:"Checkin")
        checkin["owner"] = user!.objectId
        checkin["zone"] = currentZone

        checkin.saveInBackgroundWithBlock {
            (success: Bool, error: NSError?) -> Void in
            if (success) {
                // The object has been saved.
                self.changeButtonTitle()
                self.stopShowLoading()
                
                var alertMessage = ""
                if self.imageForButtonIsCheckin == true {
                    alertMessage = "You checked out!"
                } else {
                    alertMessage = "You checked in!"
                }
                
                let alert = UIAlertController(title: "Thanks", message: alertMessage, preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
                
            } else {
                // There was a problem, check error.description
                self.stopShowLoading()
            }
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
        
    func locationManager(manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], inRegion region: CLBeaconRegion) {
        
        for b in beacons {
            print(b.proximity.hashValue)
        }
        
        let knownBeacons = beacons.filter{ $0.proximity != CLProximity.Unknown }
        
        if (knownBeacons.count > 0) {
            
            print("true")
            
            currentZone = knownBeacons[0].proximityUUID.UUIDString
            
            signInButton.enabled = true
            signInButton.alpha = 1
        
        } else {
        
            print("false")
            signInButton.enabled = false
            signInButton.alpha = 0.3
        
        }
        
    }
    
}
