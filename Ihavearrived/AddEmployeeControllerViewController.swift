//
//  AddEmployeeControllerViewController.swift
//  Ihavearrived
//
//  Created by Jonas on 27/10/15.
//  Copyright © 2015 Jonas. All rights reserved.
//

import UIKit
import Parse

class AddEmployeeControllerViewController: UIViewController, UITextFieldDelegate {

    
    @IBOutlet weak var EmployeeEmail: UITextField!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(tap)
        
        EmployeeEmail.delegate = self
        
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func checkEmailFormat() -> Bool {
        
        if (isValidEmail(EmployeeEmail.text!)) {
            return true
        } else {
            return false
        }
        
    }
    
    func isValidEmail(testStr:String) -> Bool {
    
        // println("validate calendar: \(testStr)")
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(testStr)
    
    }
    
    @IBAction func AddEmployeeButton(sender: AnyObject) {
        
        // check if already added, query for user and add
        
        if (checkEmailFormat()) {
        
            let EmployeeEmailLowerCase = EmployeeEmail.text!.lowercaseString
            let userQuery = PFUser.query()
            userQuery!.whereKey("email", equalTo:EmployeeEmailLowerCase)
            
            let currentUser = PFUser.currentUser()
            var alreadyInList = false
                
            userQuery!.getFirstObjectInBackgroundWithBlock {
                (object: PFObject?, error: NSError?) -> Void in
                if error != nil || object == nil {
                    
                    let alert = UIAlertController(title: "Ooops", message: "No employee found with that email", preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                    self.presentViewController(alert, animated: true, completion: nil)
                    
                } else {
                    
                    print("found user")
                    
                    if let employeeArray = currentUser!["employees"] as? [String] {
                        
                        for user: String in employeeArray {
                            
                            if user == object!.objectId{
                                alreadyInList = true
                                print("already in list")
                                
                                let alert = UIAlertController(title: "Oooops", message: "Already added employee!", preferredStyle: UIAlertControllerStyle.Alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                                self.presentViewController(alert, animated: true, completion: nil)
                                
                            }
                    
                        }
                        
                    } else {
                         print("nil")
                    }
                    
                    if (!alreadyInList) {
                        currentUser!.addObject(object!.objectId!, forKey: "employees")
                        currentUser!.saveInBackground()
                        
                        let alert = UIAlertController(title: "Allright", message: "Employee added!", preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                        self.presentViewController(alert, animated: true, completion: nil)
                        
                    }
                    
                }
                
            }
            
        } else {
    
            print("error uialert")
        
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
