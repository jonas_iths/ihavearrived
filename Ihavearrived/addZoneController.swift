//
//  addZoneController.swift
//  Ihavearrived
//
//  Created by Jonas on 03/11/15.
//  Copyright © 2015 Jonas. All rights reserved.
//

import UIKit
import Parse

class addZoneController: UITableViewController, UITextFieldDelegate {

    var arrayForTable: [[String]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let user = PFUser.currentUser()
        arrayForTable = user!["zones"] as! [[String]]
        tableView.reloadData()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(tap)
        
    }
    
    override func tableView(tableView: UITableView, editingStyleForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCellEditingStyle {
        
        if indexPath.row == 0 {
            return .None
        } else {
            return .Delete
        }
        
    }
    
    func alertAlreadySavedZone() {
    
        let alert = UIAlertController(title: "Ooops", message: "Zone already saved!", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    
    }
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1 + arrayForTable.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

        let cell:AddZoneCellController
        
        if indexPath.row == 0 {
            cell = tableView.dequeueReusableCellWithIdentifier("Header", forIndexPath: indexPath) as! AddZoneCellController
            cell.parent = self
            
            cell.iBeaconString.delegate = cell
            cell.iBeaconNameString.delegate = cell
            
        } else {
            cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! AddZoneCellController
            cell.iBeaconIDCell2.text = arrayForTable[indexPath.row - 1][0]
            cell.iBeaconNameCell2.text = arrayForTable[indexPath.row - 1][1]
        }

        return cell
    }
    
    override func tableView(tableView:UITableView, heightForRowAtIndexPath indexPath:NSIndexPath)->CGFloat
    {
     
        if indexPath.row == 0 {
            return 230
        } else {
            return 100
        }
        
    }

    func alertEmptyTextField() {
    
        let alertController = UIAlertController(title: "Oooops", message:
            "Fill in properly!", preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default,handler: nil))
        
        self.presentViewController(alertController, animated: true, completion: nil)
    
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */
    
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            let user = PFUser.currentUser()
            
            print(arrayForTable[0])
            print((indexPath.row - 1))
            
            arrayForTable.removeAtIndex(indexPath.row - 1)
            user!["zones"] = arrayForTable
            // sharedClass.zones.removeAtIndex(indexPath.row - 1)
            user!.saveEventually()

            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
            
            // delete zone from shared and db
            
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }

    func reload() {
        print("reloading")
        tableView.reloadData()
    }
    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}