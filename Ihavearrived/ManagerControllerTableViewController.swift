//
//  ManagerControllerTableViewController.swift
//  Ihavearrived
//
//  Created by Jonas on 26/10/15.
//  Copyright © 2015 Jonas. All rights reserved.
//

import UIKit
import Parse

class ManagerControllerTableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    var arrayTableViewSource: [String] = []
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidAppear(animated: Bool) {
        
        print("didappear")
        
        let currentUser = PFUser.currentUser()
        arrayTableViewSource = currentUser!["employees"] as! [String]
        self.tableView.reloadData()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        
        let currentUser = PFUser.currentUser()
        arrayTableViewSource = currentUser!["employees"] as! [String]
        self.tableView.reloadData()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayTableViewSource.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell") as! managerTableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        
        // Get user
        let userQuery = PFUser.query()
        userQuery!.whereKey("objectId", equalTo:arrayTableViewSource[indexPath.row])        
        
        userQuery!.getFirstObjectInBackgroundWithBlock {
            (object: PFObject?, error: NSError?) -> Void in
            
            if error != nil || object == nil {
                print("Did not find a user")
            } else {
                // print("Did FIND a user ")
                cell.title.text = object!["name"] as? String
                cell.Email.text = object!["email"] as? String
            }
            
        }
        
        cell.layoutMargins = UIEdgeInsetsZero
        
        return cell
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let indexPath = tableView.indexPathForSelectedRow!
        let currentCell = tableView.cellForRowAtIndexPath(indexPath) as! managerTableViewCell!;
        
        sharedClass.selectedPersonName = currentCell.title.text!
        sharedClass.selectedPersonId = arrayTableViewSource[indexPath.row]
        
    }

    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */
    
    // Override to support editing the table view.
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            
            arrayTableViewSource.removeAtIndex(indexPath.row)
            let user = PFUser.currentUser()
            user!["employees"].removeObjectAtIndex(indexPath.row)
            user?.saveEventually()
            
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
            
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }        

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        navigationItem.backBarButtonItem = backItem // This will show in the next view controller being pushed
        
        if (segue.identifier == "toAddEmployeeSegue") {
            // pass data to next view
        }
        if (segue.identifier == "onePersonSegue") {
            
            // sharedClass.selectedPersonName = ""
            
            
        }
        
    }
    

}
