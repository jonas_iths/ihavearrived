//
//  RegisterController.swift
//  Ihavearrived
//
//  Created by Jonas on 21/10/15.
//  Copyright © 2015 Jonas. All rights reserved.
//

import UIKit
import Parse

class RegisterController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var mailAddress: UITextField!
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var password: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        mailAddress.delegate = self
        name.delegate = self
        password.delegate = self
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func sendRegistration(sender: AnyObject) {
        
        if (userSignUpCeckifCorrect()) {
            
            userSignUp()
            
        } else {
            
            let alertController = UIAlertController(title: "Ooops", message:
                "Fill in properly please!", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default,handler: nil))
            
            self.presentViewController(alertController, animated: true, completion: nil)
            
        }
        
    }
    
    func isValidEmail(testStr:String) -> Bool {
        // println("validate calendar: \(testStr)")
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(testStr)
    }
    
    func userSignUpCeckifCorrect() -> Bool {

        if (isValidEmail(mailAddress.text!) && password.text != "" && name.text != "") {
            return true
        } else {
            return false
        }
        
    }
    
    func userSignUp() {
        
        // check if user exists and filled in properly
        
        let user = PFUser()
        user.username = mailAddress.text!.lowercaseString
        user.email = user.username
        user.password = password.text
        
        // print(sharedClass.manager)
        
        user["manager"] = sharedClass.manager
        user["name"] = name.text
        
        user.signUpInBackgroundWithBlock {
            (succeeded: Bool, error: NSError?) -> Void in
            if error == nil {
                print("Signed up")
                
                let alertController = UIAlertController(title: "Allright", message:
                    "You have signed up! You can now log in!", preferredStyle: UIAlertControllerStyle.Alert)
                
                alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default,handler: { action in
                    self.dismiss() }))
                
                self.presentViewController(alertController, animated: true, completion: nil)
                
            } else {
                if (error!.code == 202) {
                    
                    let alertController = UIAlertController(title: "Ooops", message:
                        "Email already registred", preferredStyle: UIAlertControllerStyle.Alert)
                    alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default,handler: nil))
                    
                    self.presentViewController(alertController, animated: true, completion: nil)
                
                }
                print("Error signing up")
            }
        }
        
    }

    func dismiss() {
        // self.dismissViewControllerAnimated(true, completion: nil)
        navigationController!.popViewControllerAnimated(true)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
