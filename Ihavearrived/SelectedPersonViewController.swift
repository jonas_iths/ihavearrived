//
//  SelectedPersonViewController.swift
//  Ihavearrived
//
//  Created by Jonas on 30/10/15.
//  Copyright © 2015 Jonas. All rights reserved.
//

import UIKit
import Parse

class SelectedPersonViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableviewDaily: UITableView!
    var arrayForTable: [PFObject] = []
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.title = sharedClass.selectedPersonName
        
        tableviewDaily.delegate = self
        tableviewDaily.dataSource = self
        
        reloadTable()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func reloadTable() {

        let query = PFQuery(className:"Checkin")
        
        let currentUser = PFUser.currentUser()
        var arr:[String] = []
        
        for a in currentUser!["zones"] as! [[String]] {
            arr.append(a[0])
        }
        
        query.whereKey("owner", equalTo:sharedClass.selectedPersonId)
        query.whereKey("zone", containedIn:arr)
        query.orderByDescending("createdAt")
        query.limit = 62
                        
        query.findObjectsInBackgroundWithBlock {
            (objects: [PFObject]?, error: NSError?) -> Void in
            
            if error == nil {
                // The find succeeded.
                
                if let objects = objects {
                    for object in objects {
                        self.arrayForTable.append(object)
                        self.tableviewDaily.reloadData()
                    }
                }
                
            } else {
                // Log details of the failure
                print("Error: \(error!) \(error!.userInfo)")
            }
        }
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayForTable.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell") as! selectedPersonCell
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        
        let object = arrayForTable[indexPath.row]
        
        let date = object.createdAt //get the time, in this case the time an object was created.
        //format date
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy MMM EEEE HH:mm"
        // dateFormatter.timeZone = NSTimeZone(name: "UTC")
        let dateString = dateFormatter.stringFromDate(date!)
        
        cell.date.text = dateString
    
        return cell
        
    }

}
